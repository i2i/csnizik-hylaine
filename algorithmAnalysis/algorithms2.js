'use strict';

const allStates = [
  'Alabama',        'Alaska',        'Arizona',
  'Arkansas',       'California',    'Colorado',
  'Connecticut',    'Delaware',      'Florida',
  'Georgia',        'Hawaii',        'Idaho',
  'Illinois',       'Indiana',       'Iowa',
  'Kansas',         'Kentucky',      'Louisiana',
  'Maine',          'Maryland',      'Massachusetts',
  'Michigan',       'Minnesota',     'Mississippi',
  'Missouri',       'Montana',       'Nebraska',
  'Nevada',         'New Hampshire', 'New Jersey',
  'New Mexico',     'New York',      'North Carolina',
  'North Dakota',   'Ohio',          'Oklahoma',
  'Oregon',         'Pennsylvania',  'Rhode Island',
  'South Carolina', 'South Dakota',  'Tennessee',
  'Texas',          'Utah',          'Vermont',
  'Virginia',       'Washington',    'West Virginia',
  'Wisconsin',      'Wyoming'
]

/* ************************************************************************************************
 * Problem: Implement the batch function.
 * 
 * I would like to return a stream of batches of size <= batchCount while enumerating over the
 * input items. Each batch is defined as an array where the number of elements in the array <= batchCount
 */

/**
 * Generates a list of items in batches.
 * @param {items} an array of objects
 * @param {batchSize} batchSize The batch size
 * @returns  {object[]}
 */
function* batch(items, batchSize) {
  while (items.length !== 0) { 
    let batchedItems = items.splice(0, batchSize); // CS: move n number of items to batchedItems
    yield batchedItems;
  }
}

// PROOF/TEST

console.time('batch');
let batchSize = 4; // CS: batchSize == 4 should return 12 batches of 4 and a 13th batch of 2
let statesTest = batch(allStates, batchSize); 
for (const bat of statesTest) { // CS: use the generator as an iterator
  console.log(bat);
}
console.timeEnd('batch');

