'use strict';

const fileWriter = require("../scripts/fileWriter");
const fileReader = require('../scripts/fileReader');
const fileStats = require('../scripts/fileStats');

const cache = [];

// Record should be indexed by the combination of pk_1 and pk_2 (each are part of the "primary key"). 
// loadRecordsIntoCache should replace any element in the cache with identical pk_1 and pk_2 combinations
// The order of priorities for the solution should be: Correctness, Performance, and Style.

/**
 * Returns a cache of records.
 * @param {Function} func 
 */
function loadRecordsIntoCache(func) { // CS: Each time getRecord() is passed through this function its results will be memoized.
  return function(a, b){
    const id = a.toString() + b.toString(); // CS: Combine the two parameters to make a single unique id
    if (cache[id] == undefined) { 
      cache[id] = func(a, b); // CS: If the function with these params have not been memoized yet, save the function 
    }
    return cache[id];
  };
}

// Create an efficient cache of records. Use your own data structure, rather than a caching package

/**
 * Returns a record from the cache
 * @param {Number} pk1 
 * @param {Number} pk2
 * @returns {Record}
 */
function getRecord(pk1, pk2) { // CS: NOTE: this function is inefficient by design to maximize its execution time
  const dataSet = JSON.parse(fileReader('randomRecords.json')); // CS: load all records from JSON data source
  const keys = Object.keys(dataSet);
  let result = 'No record found.';
  keys.forEach((key, index) => {
    const row = new Record(dataSet[key]._pk1, dataSet[key]._pk2, dataSet[key]._value) // CS: Each row of JSON is used to construct a new Record
    if (row.pk1 === pk1 && row.pk2 === pk2) {
      result = row.value; // CS: This is the value we want
    }
  })
  return result;
}

const getRecordCached = loadRecordsIntoCache(getRecord);

class Record {
  constructor(pk1, pk2, value) {
    this._pk1 = pk1;
    this._pk2 = pk2;
    this._value = value;
  }

  get pk1() {
    return this._pk1;
  }

  get pk2() {
    return this._pk2;
  }

  get value() {
    return this._value;
  }
}

function generateRandomRecords(values, file_name){ // CS: This function  will create a JSON file to be used as a data source in the proof/test
  let randRecords = [];
  for (let i = 0; i < values; i++){
    for (let j = 0; j < values; j++){
      const value = (Math.floor(Math.random()*1000000)+1); // Random # between 1 && 1 mil
      const randRecord = new Record(i, j, value);
      randRecords.push(randRecord);
    }
  }
  fileWriter(file_name, JSON.stringify(randRecords,null,'\t'));
}

// PROOF/TEST

generateRandomRecords(2000, 'randomRecords.json'); // This will create a data source with n^2 records. 
// CS: WARNING: this could potentially create an enormous file; I would recommend < 2000 (2000^2 generates a 227MB file).

console.time('getRecord-no cache 1'); // Search 1 without cache
const foundRecordNc1 = getRecord(1999,1999);
console.log(foundRecordNc1);
console.timeEnd('getRecord-no cache 1'); // 4802.172ms

console.time('getRecord-cached 1'); // Search 1 with cache - should take as long as without cache since this is the initial search with these params.
const foundRecordC1 = getRecordCached(1999,1999);
console.log(foundRecordC1);
console.timeEnd('getRecord-cached 1'); // 5681.992ms

console.time('getRecord-cached 2'); // Search 2 with cache - should take < 1ms since this search is memoized. 
const foundRecordC2 = getRecordCached(1999,1999);
console.log(foundRecordC2);
console.timeEnd('getRecord-cached 2'); // 0.069ms

// CS: NOTE: this method of memoization saves overhead in a module with heavy computational or repetitive functions. 