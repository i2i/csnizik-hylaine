'use strict'

const fileReader = require('../scripts/fileReader');
const fileWriter = require('../scripts/fileWriter');
const fileStats = require('../scripts/fileStats');

// Save all file lines that start with the first letter of the alphabet to a new file
// Must be able to handle VERY large files.

/**
 * 
 * @param {String} inputFilePath 
 * @param {String} outputfilePath 
 */
function saveLines(inputFilePath, outputfilePath) {
  let fileContents = fileReader(inputFilePath);
  let regEx = new RegExp(/^a.*$/,'i'); // CS: Regex pattern to match a|A at the beginning of a line
    let writeContent = '';
    fileContents.split("\n").map(line => writeContent += regEx.exec(line) !== null ? (line + '\n') : '');
    fileWriter(outputfilePath, writeContent);
}

// PROOF/TEST

console.time('saveLines');
const sourceFile = './war-and-peace.txt';
const destFile = 'a-lines.txt';

const lines = saveLines(sourceFile, destFile);
const stats = fileStats(destFile);
console.timeEnd('saveLines');

module.exports = saveLines