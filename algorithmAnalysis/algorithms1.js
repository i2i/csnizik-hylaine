'use strict';

// console.log('Example CLI Input: ' + process.argv[1]);
/* ************************************************************************************************
 * Problem: Create a function that returns whether or not a given growth rate table is valid
 * 
 * A table is valid if:
 *   It spans all weeks 1 through 53.
 *   Each row can have multiple weeks. 
 *   All rows cover a time period moving forward
 *   No overlapping weeks appear in the table. 
 *   The sum of all growth percentages in the table is equal to 1
 * 
 * Example Valid Table:
 *   Row Index   StartWeek   EndWeek     GrowthPct
 *   1           1           4           .1
 *   2           8           10          .17
 *   3           5           7           .05
 *   4           11          53          .68
 * 
 * Other notes:
 *   The table does not have a guaranteed order
 *   The order of priorities for the solution should be:
 *     - Correctness
 *     - Elegance/Style
 *     - Performance
*/

/**
 * Validates a GrowthTable object.
 * @param {GrowthTable} growthTable
 * @returns {boolean} 
 */
function isValid(growthTable) {
  let weeks = 0;
  let growth = 0;
  for (let i in growthTable.rows){
    weeks += growthTable.rows[i].endWeek - growthTable.rows[i].startWeek + 1;
    growth += growthTable.rows[i].growthPct; // 
  }
  return (weeks === 53 && growth === 1) ? true : false;
}

class GrowthTable {
  constructor(rows) {
    this._rows = rows;
  }
  
  get rows() {
    return this._rows;
  }
}

class GrowthTableRow {
  constructor(startWeek, endWeek, growthPct) {
    this._startWeek = startWeek;
    this._endWeek = endWeek;
    this._growthPct = growthPct;
  }

  get startWeek() {
    return this._startWeek;
  }
  
  get endWeek() {
    return this._endWeek;
  }

  get growthPct() {
    return this._growthPct;
  }
}

// PROOF/TEST:

const sampleRows = [
  new GrowthTableRow(1,4,0.1),
  new GrowthTableRow(8,10,0.17),
  new GrowthTableRow(5,7,0.05),
  new GrowthTableRow(11,53,0.68)
]
const sampleTable = new GrowthTable(sampleRows);
console.log('***Testing algorithm1.js using data in sampleRows[]');
console.time('isValid');
console.log('Sample data is valid: ', isValid(sampleTable));
console.timeEnd('isValid');

module.exports = isValid;