'use strict';

/**
 * Returns a list of people by last name and then by first name.
 * 
 * @param {Array<Person>} people
 * @returns {Array<String>}
 */
function sortByLastNameThenFirstName(people) {
  // Write code to return a list of strings sorted by last name, 
  // then first name, then age. Use the format: `${LastName} - ${FirstName} - ${Age}`
  // Example string: Burgundy - Ron - 43
  const sorted = people.sort((a, b) => {
    const lNameA = a.lastName.toUpperCase();
    const lNameB = b.lastName.toUpperCase();
    const fNameA = a.firstName.toUpperCase();
    const fNameB = b.firstName.toUpperCase();
    const ageA = a.age;
    const ageB = b.age;
    return lNameA < lNameB ? -1 : lNameA > lNameB ? 1 : // CS: Sort by last name
      fNameA < fNameB ? -1 : fNameA > fNameB ? 1 : // CS: If last names are equal, sort by first name
      ageA < ageB ? -1 : ageA > ageB ? 1 : 0; // CS: If last && first are equal, sort by age
  })
  return sorted.map(person => `${person.lastName} - ${person.firstName} - ${person.age}`);
}


/**
 * 
 * @param {Array<Person>} people 
 * @returns {Array<String>}
 */
function countsByFirstName(people) {
  // Write code to return a list of strings string with a count of the number of times each first name occurs, ordered by occurence.
  // Use this format: `${name}, ${count}`
  // When printed the output should look like this:
  // Example string: Ron, 2
  const firstNames = [];
  const unsortedUniques = [];

  people.map(person => firstNames.push(person.firstName));

  const uniqueNames = firstNames.filter((x, i, a) => a.indexOf(x) ==i)
  const countOccurences = (array, value) => {
    return array.reduce((accumulator, element) => {
      return (value === element ? accumulator + 1 : accumulator)
    }, 0);
  }
  uniqueNames.map((uName, i) => {
    const u = {
      name: uName, 
      count: countOccurences(firstNames, uniqueNames[i])
    }
    unsortedUniques.push(u);
  })
  const sortedUniques = unsortedUniques.sort((a, b) => 
    a.count < b.count ? 1 : 
    a.count > b.count ? -1 : 0);
  return sortedUniques.map(name => `${name.name}, ${name.count}`);
}

class Person {
  constructor(firstName, lastName, age) {
    this._firstName = firstName;
    this._lastName = lastName;
    this._age = age;
  }

  get firstName() {
    return this._firstName;
  }
  
  get lastName() {
    return this._lastName;
  }

  get age() {
    return this._age;
  }
}

// PROOF/TEST

const sampleData = [
  new Person('Amy', 'Monroe', 54),
  new Person('Amy', 'Duval', 54),
  new Person('Joe', 'Conrad', 14),
  new Person('Amy', 'Jenkins', 34),
  new Person('Bill', 'Monroe', 34),
  new Person('Amy', 'Smith', 34),
  new Person('Joe', 'Johnson', 14),
  new Person('Tom', 'Johnson', 28),
  new Person('Tim', 'Johnson', 28),
  new Person('Bill', 'Johnson', 27),
  new Person('Amy', 'Duval', 38),
  new Person('Amy', 'Duval', 66),
  new Person('Amy', 'Duval', 8),
  new Person('Bill', 'Johnson', 39),
  new Person('Bill', 'Williams', 14),
  new Person('Amy', 'Henderson', 22)
]
console.time('sortByLastNameThenFirstName');
console.log(sortByLastNameThenFirstName(sampleData));
console.timeEnd('sortByLastNameThenFirstName');

console.time('countsByFirstName');
console.log(countsByFirstName(sampleData));
console.timeEnd('countsByFirstName');

