'use strict'

const fs = require('fs');

/**
 * Reads a file's contents
 * @param  {String} file_name 
 * @returns {Object} File's contents
 */
function fileReader(file_name){
	if(fs.existsSync(file_name)) {
    return fs.readFileSync(file_name, 'utf-8')
	}else{
		throw "File is missing.";
	}
}

module.exports = fileReader;