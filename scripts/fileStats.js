'use strict'

const fs = require('fs');

/**
 * Tests for file's existence; prints file size
 * @param  {String} file_name
 */
function fileStats(file_name) {
    fs.open(file_name, 'r', (err, fd) => {
      if (err) throw err;
      fs.fstat(fd, (err, stats) => {
        if (err) throw err;
        console.log('FILE CREATED:\nName:', file_name)
        console.log('Size: ', (stats.size/1000).toFixed(2), 'kB');
        fs.close(fd, (err) => {
          if (err) throw err;
        })
      })
    })
  }

  module.exports = fileStats;