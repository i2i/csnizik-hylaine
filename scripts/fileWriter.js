'use strict'

const fs = require('fs');

/**
 * Writes contents to a file
 * @param {String} file_name 
 * @param {String} content 
 */

function fileWriter(file_name, content) {
    fs.writeFileSync(file_name, content, function(err) {
        if (err) {
            throw err;
        }
    });
}

module.exports = fileWriter;