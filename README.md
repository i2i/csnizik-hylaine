# Hylaine screen

## Christopher Snizik

[csnizik@tulane.edu](csnizik@tulane.edu)

### /scripts/

I wrote three utility modules that I reused on multiple projects. Each of them builds on the Node.js `fs` module for interacting with the file system.

1. `fileReader.js` - uses `fs.existsSync()` and `fs.readFileSync()` to check for a file's existence and read its contents. Used in `crosswordSolver.js`, `algorithm4.js` and `algorithm5.js`.
1. `fileStats.js` - uses `fs.existsSync()`, `fs.open()`, `fs.fstat()` and `fs.close()` to check for a file's existence, open the file, log the file's size to the console, and close the file. Used in `algorithm4.js` and `algorithm5`.js.
1. `fileWriter.js` - uses `fs.writeFileSync()` to write content to the file that was opened using `fileReader.js`. Used in `algorithm4.js` and `algorithm5.js`.

## Project Files

---

### crosswordSolver

- Accepts user input for a pattern (letters and \* for wildcards) from stdin.

- Loads and parses the contents of `english.csv`.

- Compares the pattern given with the array of words and returns an array with all matching words, along with a count of the returned words.

#### PROOF/TEST

Evaluate the script with `npm run crossword`.

#### NOTE

I wasn't clear on why line 3 was added to the test. Was the tabular data supposed to be provided as arguments to the evaluation script? With this script (and the next 4) I included the sample data either within the script or in external files. -CS

---

### algorithm1.js - Growth Table Validator

1. `isValid()` takes a `growthTable` object as an argument, which has rows of class `GrowthTableRow`. Each row has an index and three values:

   1. `startWeek`
   1. `endWeek`
   1. `growthPct`

1. The function iterates over the table's rows and calculates the number of weeks and the total growth amount. It returns `true|false` depending on the validity of the calculated totals.

#### PROOF/TEST

Evaluate the script with `npm run alg1`.

---

### algorithm2.js - Batch Generator

1. Generator function `batch()` accepts two arguments: an array of objects and a batch size.
1. `batch()` is called as an iterator to yield batches of items in the desired batch size (or less when necessary).

#### PROOF/TEST

Evaluate the script with `npm run alg2.js`. An array of the 50 states' names is included in the script, and a value of 4 is set for `batchSize` on line 46.

---

### algorithm3.js - People Sorting

1. This script has two functions:
   1. `sortByLastNameThenFirstName()` accepts an array of `Person` objects as an argument and returns it sorted by three criteria, in this order: last name, first name, age.
   1. `countsByFirstName()` accepts an array of `Person` objects as an argument and returns a list of strings with the unique first names and the number of occurrences of each name in the array; this is returned sorted by number of occurrences.

#### PROOF/TEST

Evaluate the script with `npm run alg3`. The sample data that was provided is used, along with some additional names to demonstrate the sorting and counting functionality.

---

### algorithm4.js - Lines Beginning With 'A'

1. Reads the contents of a file, finds all lines in the file that begin with the letter 'a', then writes the found content to a newly created file.
1. Accepts `inputFilePath` and `outputfilePath` as arguments.

#### PROOF/TEST

Evaluate the script with `npm run alg4`. To demonstrate that the script can handle very large files, a text file with the contents of the book _War and Peace_ by Leo Tolstoy is included in the project files as `war-and-peace.txt`. The generated file will be named `a-lines.txt` and saved in the same directory.

---

### algorithm5 - Caching/Memoization

1. Memoizes the results of a function that is designed to have a high overhead. This is useful in cases where a script has heavy/expensive calculations or repetitive function calls.
1. The script uses two functions:
   1. `getRecord()` is the main function used to parse a dataset of `Record` objects, search for a unique record that matches the two arguments `pk1` and `pk2`, and returns the `value` of the matching record if found. **NOTE**: This function could be written more efficiently, but because the purpose of this exercise is to demonstrate the reduced overhead using memoization, I purposefully left the inefficiencies.
   1. `loadRecordsIntoCache()` is the memoization function. It accepts a function as an argument, checks to see if the current function (with its two current arguments) has already run, and if so returns the stored values. If this function/argument combination has not run, the memoization function allows it to run and stores the value.

#### PROOF/TEST

Evaluate the script with `npm run alg5`. The evaluation will do the following:

1. Generate and save a new file, `randomRecords.json`, which will have _n^2_ records. Each record will have a unique combination of `pk1` and `pk2` values, plus a random number between 1 - 1,000,000 for the `value`. An initial value of 2000 is provided, which will generate a `.json` file with 4 million records (~227MB file size).
1. Run `getRecord()` against this data set 3 times:
   1. With no caching;
   1. With caching (first run);
   1. With caching (second run);
1. The runtime for each of the three tests will be displayed. On my machine, the first and second tests took 4802ms and 5681ms respectively; the third test (which is the first one that has access to the memoized data) took 0.069ms.
